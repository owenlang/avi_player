#include <iostream>
#include <opencv2/opencv.hpp>
using namespace cv;
using namespace std;

int main(int argc, char *argv[])
{
    cvNamedWindow("Example2", CV_WINDOW_AUTOSIZE);
    CvCapture *capture = cvCreateFileCapture(argv[1]);
    IplImage *frame;
    while (1) {
        frame = cvQueryFrame(capture);
        if (!frame) {
            break;
        }
        cvShowImage("Example2", frame);
        char c = cvWaitKey(33);
        if (27 == c) {
            break;
        }
    }
    cvReleaseCapture(&capture);
    cvDestroyWindow("Example2");


    return 0;
}

